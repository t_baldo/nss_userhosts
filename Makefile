CC=gcc
CFLAGS=-Wall -Wextra -Werror -O2 -g
LDFLAGS=-Wall -Wextra -Werror -O2 -g

all: out/lib32/libnss_userhosts.so out/lib64/libnss_userhosts.so out/bin32/check out/bin64/check

out/lib32/libnss_userhosts.so: out/obj32/userhosts.o
	@mkdir -p out/lib32
	$(CC) $(LDFLAGS) -m32 -shared -o $@ $<

out/obj32/userhosts.o: userhosts.c
	@mkdir -p out/obj32
	$(CC) $(CFLAGS) -m32 -fPIC -c -o $@ $<

out/lib64/libnss_userhosts.so: out/obj64/userhosts.o
	@mkdir -p out/lib64
	$(CC) $(LDFLAGS) -m64 -shared -o $@ $<

out/obj64/userhosts.o: userhosts.c
	@mkdir -p out/obj64
	$(CC) $(CFLAGS) -m64 -fPIC -c -o $@ $<

out/bin32/check: check.c
	@mkdir -p out/bin32
	$(CC) $(CFLAGS) -m32 -o $@ $<

out/bin64/check: check.c
	@mkdir -p out/bin64
	$(CC) $(CFLAGS) -m64 -o $@ $<

clean:
	/bin/rm -fr out

install: all
	sudo ./install.sh

uninstall: all
	sudo ./uninstall.sh

update: all
	sudo ./update_nsswitch.sh

cleanup: all
	sudo ./cleanup_nsswitch.sh

run: all
	out/bin32/check
	out/bin64/check

help:
	@echo "make install"
	@echo "    install libraries"
	@echo "make update"
	@echo "    update \"hosts\" entry in /etc/nsswitch.conf"
	@echo "make uninstall"
	@echo "    remove libraries"
	@echo "make cleanup"
	@echo "    cleanup /etc/nsswitch.conf"

new: clean all
