#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>

/*****************************************************************************/
#include <nss.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/*****************************************************************************/
typedef struct {
    struct in_addr      addr;
    void*               addr_list[2];
    void*               aliases[1];
    char*               name;
} userhost_t;

/*****************************************************************************/
static int verbose = 0;

/*****************************************************************************/
#if 0
enum nss_status _nss_userhosts_gethostent_r (struct hostent *result, char *buffer, size_t buflen, int *errnop)
{
    printf("%s()\n", __func__);
    return NSS_STATUS_NOTFOUND;
}
#endif

/*****************************************************************************/
static char* get_field(char* line, char** next)
{
    char*       p0;
    char*       p1;

    assert(line != NULL);
    assert(next != NULL);

    p0 = line;

    for(;;) {
        if (*p0 == 0) {
            p0 = NULL;
            goto out;
        }
        if (isspace(*p0)) {
            p0++;
        } else {
            break;
        }
    }
    p1 = p0;
    for(;;) {
        if (*p1 == 0) {
            p0 = NULL;
            goto out;
        }
        if (!isspace(*p1)) {
            p1++;
        } else {
            break;
        }
    }
    *p1 = 0;
    *next = (p1 + 1);
out:
    return p0;
}

/*****************************************************************************/
static int lookup_r(
    const char*     name,
    int             field,
    char*           line,
    size_t          size,
    char**          result_p)
{
    int             ret = -1;
    FILE*           fhd = NULL;
    const char*     home;
    char*           p0;
    char*           p1;
    char*           p2;
    char            filename[FILENAME_MAX];
    int             lineno = 1;

    assert(name != NULL);
    assert(field >= 0);
    assert(field <= 1);
    assert(line != NULL);
    assert(size > 0);
    assert(result_p != NULL);

    home = getenv("HOME");
    if (home == NULL) {
        goto out;
    }

    snprintf(filename, sizeof(filename), "%s/.hosts", home);

    fhd = fopen(filename, "rb");
    if (fhd == NULL) {
        goto out;
    }
    while ((fgets(line, size, fhd)) != NULL) {
        if (line[0] == '#') {
            continue;
        }
        p0 = get_field(line, &p1);
        if (p0 == NULL) {
            fprintf(stderr, "Syntax error line %d in %s\n", lineno, filename);
            goto out;
        }
        p1 = get_field(p1, &p2);
        if (p1 == NULL) {
            fprintf(stderr, "Syntax error line %d in %s\n", lineno, filename);
            goto out;
        }
        if (verbose) {
            printf("<%s><%s>\n", p0, p1);
        }
        switch(field) {
        case 0:
            if (!strcmp(p0, name)) {
                *result_p = p1;
                ret = 0;
                goto out;
            }
            break;
        case 1:
            if (!strcmp(p1, name)) {
                *result_p = p0;
                ret = 0;
                goto out;
            }
            break;
        }
        lineno++;
    }

out:
    if (fhd != NULL) {
        fclose(fhd);
    }
    return ret;
}

/*****************************************************************************/
enum nss_status _nss_userhosts_gethostbyname2_r(
    const char*     name,
    int             af,
    struct hostent* result,
    char*           buffer,
    size_t          buflen,
    int*            errnop,
    int*            herrnop)
{
    enum nss_status     ret = NSS_STATUS_NOTFOUND;
    in_addr_t           addr_value;
    userhost_t*         userhost;
    char                line[256];
    char*               addr_str;

    if (getenv("USERHOSTS_DEBUG")) {
        verbose = 1;
    }

    if (verbose) {
        printf("%s(%s, %d, result=%p, buffer=%p, %d, %p, %p)\n", __func__, name, af, result, buffer, (int)buflen, errnop, herrnop);
    }
    if (buflen < sizeof(*userhost) + strlen(name)+1) {
        goto out;
    }
    if (lookup_r(name, 1, line, sizeof(line), &addr_str) < 0) {
        goto out;
    }
    {
        int     res;
        struct hostent* result_p;
        res = gethostbyname_r(addr_str, result, buffer, buflen, &result_p, herrnop);
        if (res == 0) {
            ret = NSS_STATUS_SUCCESS;
            goto out;
        }
    }
    addr_value = inet_addr(addr_str);

    if (addr_value == INADDR_NONE) {
        goto out;
    }

    userhost = (userhost_t*)buffer;
    userhost->addr.s_addr   = addr_value;
    userhost->addr_list[0]  = &userhost->addr;
    userhost->addr_list[1]  = NULL;
    userhost->aliases[0]    = NULL;
    userhost->name          = (char*)(userhost + 1);
    strcpy(userhost->name, name);

    result->h_name      = userhost->name;
    result->h_aliases   = (void*)&userhost->aliases;
    result->h_addr_list = (void*)&userhost->addr_list;
    result->h_addrtype  = AF_INET;
    result->h_length    = 4;

    ret = NSS_STATUS_SUCCESS;

out:
    if (ret == NSS_STATUS_NOTFOUND) {
        if (errnop != NULL) {
            *errnop = ENOENT;
        }
    }
    return ret;
}

/*****************************************************************************/
enum nss_status _nss_userhosts_gethostbyname_r(
    const char*     name,
    struct hostent* result,
    char*           buffer,
    size_t          buflen,
    int*            errnop,
    int*            herrnop)
{
//    printf("%s(%s)\n", __func__, name);
    return _nss_userhosts_gethostbyname2_r(name, AF_INET, result, buffer, buflen, errnop, herrnop);
}

/*****************************************************************************/
enum nss_status _nss_userhosts_gethostbyaddr_r(
    const void*     addr,
    socklen_t       len,
    int             type,
    struct hostent* result,
    char*           buffer,
    size_t          buflen,
    int*            errnop,
    int*            herrnop)
{
    enum nss_status             ret = NSS_STATUS_NOTFOUND;
    const struct in_addr*       in_addr = addr;
    char                        line[256];
    char*                       addr_str;
    userhost_t*                 userhost;

    (void)len;
    (void)herrnop;

    if (getenv("USERHOSTS_DEBUG")) {
        verbose = 1;
    }

    if (verbose) {
        printf("%s(type=%d)\n", __func__, type);
    }
    if (type != AF_INET){
        goto out;
    }
//    printf("%s\n", inet_ntoa(*in_addr));
    if (lookup_r(inet_ntoa(*in_addr), 0, line, sizeof(line), &addr_str) < 0) {
        goto out;
    }
    if (buflen < sizeof(*userhost) + strlen(addr_str)+1) {
        goto out;
    }

    userhost = (userhost_t*)buffer;
    userhost->addr          = *in_addr;
    userhost->addr_list[0]  = &userhost->addr;
    userhost->addr_list[1]  = NULL;
    userhost->aliases[0]    = NULL;
    userhost->name          = (char*)(userhost + 1);
    strcpy(userhost->name, addr_str);

    result->h_name      = userhost->name;
    result->h_aliases   = (void*)&userhost->aliases;
    result->h_addr_list = (void*)&userhost->addr_list;
    result->h_addrtype  = AF_INET;
    result->h_length    = 4;

    ret = NSS_STATUS_SUCCESS;
out:
    if (ret == NSS_STATUS_NOTFOUND) {
        if (errnop != NULL) {
            *errnop = ENOENT;
        }
    }
    return ret;
}
