#!/bin/bash

function libdir()
{
  dirname `ldd $1 | awk '/libc.so.6/ {print $3}'`
}

function uninstall_it()
{
  file="$1"
  [ -e $file ] && echo "Removing $file" && /bin/rm -f $file || true
}

function uninstall()
{
  file="$1"
  dir=`libdir $file`
  uninstall_it $dir/libnss_userhosts.so.2
  uninstall_it $dir/libnss_userhosts.so
}

set -e

uninstall out/lib32/libnss_userhosts.so
uninstall out/lib64/libnss_userhosts.so
