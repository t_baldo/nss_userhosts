#!/bin/bash

echo "Removing \"userhosts\" at \"hosts\" entry"
sed -i '/^hosts:/s;userhosts;;g' /etc/nsswitch.conf
