#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include <netdb.h>
#include <arpa/inet.h>

static void display(const char* name)
{
    struct hostent*     ent;
    struct in_addr**    addr_list;
    char**              aliases;
    char*               alias;
    struct in_addr*     addr;
    int                 i;

    printf("######################################\n");
    printf("%s(%s)\n", __func__, name);
    ent = gethostbyname(name);
    if (ent != NULL) {
        printf("  h_name=%p=%s\n", ent->h_name, ent->h_name);
        printf("  h_aliases=%p\n", ent->h_aliases);
        aliases = (char**)ent->h_aliases;
        if (aliases != NULL) {
            i = 0;
            while ((alias = aliases[i]) != NULL) {
                printf("    %s\n", alias);
                i++;
            }
        }
        printf("  h_addrtype=%d\n", ent->h_addrtype);
        printf("  h_length=%d\n", ent->h_length);
        printf("  h_addr_list=%p\n", ent->h_addr_list);
        addr_list = (struct in_addr **)ent->h_addr_list;
        if (addr_list != NULL) {
            i = 0;
            while ((addr = addr_list[i]) != NULL) {
                printf("    %s\n", inet_ntoa(*addr));
                i++;
            }
        }
    } else {
        printf("errno=%d (%s)\n", errno, strerror(errno));
    }
}

int main(void)
{
    display("localhost");
    display("n25");
    display("n26");
    display("doesnotexist");

    return 0;
}
