#!/bin/bash

function libdir()
{
  dirname `ldd $1 | awk '/libc.so.6/ {print $3}'`
}

function install_it()
{
  file="$1"
  dir=`libdir $file`
  echo "Copying $file in $dir/libnss_userhosts.so.2"
  install --mode=0755 $file $dir/libnss_userhosts.so.2
  echo "Symlink $dir/libnss_userhosts.so to libnss_userhosts.so.2"
  ln -sf libnss_userhosts.so.2 $dir/libnss_userhosts.so
}

set -e

install_it out/lib32/libnss_userhosts.so
install_it out/lib64/libnss_userhosts.so
