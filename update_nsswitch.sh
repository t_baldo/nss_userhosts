#!/bin/bash

sed -i '/^hosts:/s;userhosts;;g' /etc/nsswitch.conf

echo "Adding \"userhosts\" at \"hosts\" entry"
sed -i '/^hosts:/s;:;:userhosts;' /etc/nsswitch.conf
