#!/bin/bash

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

FILE="$HOME/.hosts"

usage()
{
  echo "Usage: $1 [-h] [-l | -d <host> | [-a] <host> <alias> | <host> <ip>]"
  echo "  -h: this help"
  echo "  -l: list known hosts"
  echo "  -d <host>: delete one host"
  echo "  -a <host> <alias>: add host by alias"
  echo "  <host> <ip>: add host by ip"
}

action="a"
host=
ip=
with_alias=

set -- `getopt hld:a $@`
while [ $# -gt 0 ]; do
  case $1 in
    "-h") usage $0; exit 0;;
    "-l") action="l"; shift;;
    "-d") action="d"; host=$2; shift 2;;
    "-a") with_alias="1"; shift;;
    "--") shift; break;;
  esac
done

if [ "$action" = "l" ]; then
  [ ! -r $FILE ] && echo "Cannot find $FILE" && exit 1
  cat $FILE
elif [ "$action" = "d" ]; then
  [ ! -e $FILE ] && echo "Cannot find $FILE" && exit 1
  sed -i '/.* *'$host'/d' $FILE
elif [ "$action" = "a" ]; then
  host=$1
  ip=$2
  [ -z "$ip" ] && usage $0 && exit 1
  if [ -z "$with_alias" ]; then
    valid_ip $ip
    [ "$?" != "0" ] && echo "IP address $ip is not valid" && exit 1
  fi
  [ -e $FILE ] && sed -i '/.* *'$host'/d' $FILE
  echo "$ip $host" >> $FILE
#sed '/.* *'$host'/s/\(.*\) /'$ip'/' $FILE
fi
